/**
 * Created by Aleksey Starzhinskiy on 23.03.2020.
 */
import { NgModule } from '@angular/core';
import {
  RouterModule, Routes,
} from '@angular/router';

import { MainComponent} from './socialapi/main/main.component';


const Routes: Routes = [
  {
    path: '',
    component: MainComponent ,
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      Routes,
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
