import { Component, OnInit } from '@angular/core';
import {RestService} from "@services/rest.service";
import { FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';
@Component({
  selector: 'app-github-api',
  templateUrl: './github-api.component.html',
  styleUrls: ['./github-api.component.css']
})
export class GithubApiComponent implements OnInit {
  public data;
  private noData = false;
  private tabs: any = {}
  private formFilter;
  private sortCommit = [{"item": "author-date", "text":"дате создания"},{"item": "committer-date", "text":"дате коммита"}];
  private sortRepo = [{"item": "stars", "text":"рейтинг"},{"item": "forks", "text":"forks"}];
  constructor(private rest: RestService, private fb: FormBuilder) {

  }
  
  findRepoData() {
      const path = this.tabs.active === 'repo' ? 'repositories' : 'commits';
      const repoForm = this.formFilter.get(this.tabs.active);
      let objParams: {q: string, sort?: string} = {q : repoForm.value.name};
      if ( repoForm.value.sort) {
        objParams.sort = repoForm.value.sort;
      }
  
      this.rest.getGithubData(path, objParams).subscribe((data: any) => {
        if (data.items && data.items.length > 0) {
          this.data = data.items;
        } else {
          this.data = [];
        }
        this.noData =  data.items.length === 0;
      });
  }
  
  resetState() {
      this.noData = false;
      this.data = [];
  }
  showTabContent(ev) {
       this.resetState();
       const attr = ev.target.dataset.item;
       if (attr) {
          for (let i in this.tabs) {
            this.tabs[i] = false;
          }
          this.tabs[attr] = true;
          this.tabs.active = attr;
       }
  }
  
  ngOnInit() {
      this.tabs.repo = true;
      this.tabs.active = 'repo';
      this.formFilter = new FormGroup({
        repo: this.fb.group({
          name: ['',  [Validators.required, Validators.minLength(3)]],
          sort: [this.sortRepo[0].item],
        }),
        commit: this.fb.group({
          name: ['',  [Validators.required, Validators.minLength(3)]],
          sort: [this.sortCommit[0].item],
        }),
        // optional: this.fb.array(optParam)
      });
  }

}
