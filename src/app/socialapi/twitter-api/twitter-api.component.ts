import { Component, OnInit } from '@angular/core';
import {RestService} from '@services/rest.service';
@Component({
  selector: 'app-twitter-api',
  templateUrl: './twitter-api.component.html',
  styleUrls: ['./twitter-api.component.css']
})
export class TwitterApiComponent implements OnInit {

  constructor(private rest: RestService) { }

  ngOnInit() {
    this.rest.getTwitterData().subscribe((data: any) => {})
  }

}
