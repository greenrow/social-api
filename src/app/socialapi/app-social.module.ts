/**
 * Created by Aleksey Starzhinskiy on 23.03.2020.
 */
import { NgModule } from '@angular/core';
import { YoutubeApiComponent  } from './youtube-api/youtube-api.component';
import { MainComponent } from './main/main.component';
import {AppSocialRoutingModule} from './app-social-routing.module';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { GithubApiComponent } from './github-api/github-api.component';
import { TwitterApiComponent } from './twitter-api/twitter-api.component';

@NgModule({
  declarations: [YoutubeApiComponent, MainComponent, GithubApiComponent, TwitterApiComponent],
  imports: [AppSocialRoutingModule, ReactiveFormsModule, FormsModule, CommonModule ],
  exports: [
   MainComponent
  ]
})
export class AppSocialModule {}
