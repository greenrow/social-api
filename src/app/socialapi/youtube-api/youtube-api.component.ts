import { Component, OnInit } from '@angular/core';
import {RestService} from "@services/rest.service";
import { FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';
@Component({
  selector: 'app-youtube-api',
  templateUrl: './youtube-api.component.html',
  styleUrls: ['./youtube-api.component.css']
})
export class YoutubeApiComponent implements OnInit {
    public data;
    public noData = false;
    private tabs: any = {};
    private formFilter;
    showFilter = false;
    private channelsTypes: any = [{item: 'ID', text: 'ID'}, {item: 'Name', text: 'название'}];
    private optionalParams: any = ['statistics', 'contentDetails'];
    private showStatistic = false;
    private showPublicationTime = false;
  
  constructor( private rest: RestService, private fb: FormBuilder ) {
     window['s'] = this;
  }
  initSelect() {
      const channels = this.formFilter.get('channels');
      channels.get('name').reset();
      this.showFilter = channels.get('type').value === '';
  
      if (!this.showFilter) {
        channels.get('name').enable();
      } else {
        channels.get('name').disable();
      }
  }
  
  showTabContent(ev) {
      // reset data
      this. resetState();
      const attr = ev.target.dataset.item;
      if (attr) {
        for (let i in this.tabs) {
          this.tabs[i] = false;
        }
        this.tabs[attr] = true;
        this.tabs.active = attr;
      }
      if (this.tabs.active === 'videos') {
        this.findData();
      }
  }
  findData() {
      const optPar: any = [];
      const path = this.tabs.active === 'channels' ? 'channels' : 'videos';
      const form =  this.formFilter.get(path);
      const formObject = form.value;
      const objParams: {id?: number, forUsername?: string} = {};
  
      if (path === 'channels') {
        if (Object.keys(formObject).length > 0) {
          formObject.type === 'Name' ? objParams.forUsername = formObject.name :  objParams.id = formObject.name;
        }
        if (form.get('optional').controls.length > 0) {
          form.get('optional').controls.filter((el, i) => {
            if (el.value) {
              optPar.push(this.optionalParams[i]);
            }
          });
        }
      }
      if (path === 'videos') {
         if (form.value.statistic) {
           optPar.push('statistics');
         }
      }
  
      this.rest.getYouTubedata(path, objParams, optPar).subscribe((data: any) => {
        if (data.items && data.items.length > 0) {
             this.data = data.items;
        } else {
          this.data = [];
        }
        this.noData = data.items.length === 0;
      });
  }
  
  resetState() {
      this.noData = false;
      this.data = [];
  }
  
  ngOnInit() {
      this.tabs.channels = true;
      this.tabs.active = 'channels';

      // form instance
      const optParam =  this.optionalParams.map(() => {
        return this.fb.control({value: '', disabled: true});
      });
  
      this.formFilter = new FormGroup({
        channels: this.fb.group({
          name: ['',  [Validators.required, Validators.minLength(3)]],
          type: [this.channelsTypes[1].item, Validators.required],
          optional: this.fb.array(optParam)
        }),
        videos: this.fb.group({
          statistic: []
        })
      });
      const channels = this.formFilter.get('channels');
      channels.get('name').valueChanges.subscribe((el) => {
        if (channels.get('name').valid) {
          channels.get('optional').enable();
        } else {
          channels.get('optional').disable();
        }
      });
  }

}
