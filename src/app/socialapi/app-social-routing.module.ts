/**
 * Created by Aleksey Starzhinskiy on 23.03.2020.
 */
/**
 * Created by Aleksey Starzhinskiy on 23.03.2020.
 */
import { NgModule } from '@angular/core';
import {
  RouterModule, Routes,
} from '@angular/router';
import { YoutubeApiComponent } from './youtube-api/youtube-api.component';
import { GithubApiComponent } from './github-api/github-api.component';
import { TwitterApiComponent } from './twitter-api/twitter-api.component';

const Routes: Routes = [
  {
    path: 'youtube-api',
    component: YoutubeApiComponent,
  },
  {
    path: 'github-api',
    component: GithubApiComponent,
  },
  {
    path: 'twitter-api',
    component: TwitterApiComponent,
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(
      Routes,
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppSocialRoutingModule {}
