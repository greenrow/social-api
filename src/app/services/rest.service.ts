import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor( private http: HttpClient) {

  }
  
  getYouTubedata(path, paramToSend: any = {}, optPar = null) {
    paramToSend.key = "AIzaSyDTW5r1xj7zf36g6pI2RLBUv3BoPPiZJhA";
    paramToSend.part = ' contentDetails, snippet';
    
    if (path === 'channels') {
      paramToSend.part += ', contentOwnerDetails, brandingSettings ';
      if (optPar) {
        optPar.forEach((el) => {
          paramToSend.part += `,${el}`;
        });
      }
    }
    
    if (path === 'videos') {
      paramToSend.chart = 'mostPopular';
      if (optPar) {
        optPar.forEach((el) => {
          paramToSend.part += `,${el}`;
        });
      }
    }
    return this.http.get(`https://www.googleapis.com/youtube/v3/${path}`, {  params : paramToSend}, );
  }
  
  getGithubData( path = '', paramToSend: any = {}) {
    const parObj = {
      headers: new HttpHeaders({
        Accept: 'application/vnd.github.cloak-preview'
      }),
      params : paramToSend
    };
    return this.http.get(`https://api.github.com/search/${path}`, parObj);
  }
  
  getTwitterData( path = '', paramToSend: any = {}) {
    const parObj = {
      params : paramToSend
    };
    return this.http.get(`https://api.twitter.com/1.1/trends/place.json`, parObj);
  }
}
